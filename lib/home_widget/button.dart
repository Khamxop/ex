import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ICONBUTTON extends StatelessWidget {
  final Icon iconBT;
  final Function() onPressed;

  const ICONBUTTON({
    Key? key,
    required this.iconBT,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 75, 75, 75),
        borderRadius: BorderRadius.circular(50),
      ),
      height: 55,
      width: 55,
      child: IconButton(
        onPressed: onPressed,
        icon: iconBT,
        color: Colors.white,
      ),
    );
  }
}
