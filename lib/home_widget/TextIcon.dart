import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TextIcon extends StatelessWidget {
  final String text;
  const TextIcon({ Key? key,required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.notoSansLao(fontSize: 12, color: Colors.white),
    );
  }
}