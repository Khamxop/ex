import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mahazon/home_widget/NormalText.dart';
import 'package:mahazon/home_widget/TextIcon.dart';
import 'package:mahazon/home_widget/button.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final controller = CarouselController();
  final _controller = PageController();
  int ActiveIndex = 0;

  final _imagePaths = [
    'assets/image/rose.png',
    'assets/image/rose.png',
    'assets/image/rose.png',
    'assets/image/rose.png',
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/image/bg.jpeg"),
            fit: BoxFit.fill,
          ),
        ),
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.transparent,
            appBar: PreferredSize(
              child: AppBar(
                title: Text("Mahazon"),
                leading: Icon(
                  Icons.account_circle_outlined,
                  size: 50,
                ),
                actions: [Icon(Icons.notifications_none_rounded)],
                backgroundColor: Colors.transparent,
                elevation: 0,
                flexibleSpace: SafeArea(
                    child: Column(
                  children: [
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 65,
                        ),
                        TextButton(
                          onPressed: () {},
                          child: Text(
                            "sign in >",
                            style: GoogleFonts.notoSansLao(
                                fontSize: 12, color: Colors.white),
                          ),
                        )
                      ],
                    )
                  ],
                )),
              ),
              preferredSize: Size.fromHeight(75),
            ),
            body: SingleChildScrollView(
              child: Column(
                children: [
                  //code title
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 25,
                        width: 5,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      NormalText(text: "Follow"),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 12, right: 12),
                    height: 1,
                    width: double.infinity,
                    color: Colors.white,
                  ),

                  // end code titile
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 0, right: 0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius: BorderRadius.circular(10)),
                          height: 40,
                          width: 300,
                          margin: EdgeInsets.only(left: 20, right: 0),
                          child: TextField(
                            decoration: InputDecoration(
                                prefixIcon: Icon(
                                  Icons.search,
                                  size: 35,
                                  color: Color.fromARGB(255, 75, 75, 75),
                                ),
                                fillColor: Colors.white,
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                    width: 0,
                                    style: BorderStyle.none,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                      const Radius.circular(10)),
                                ),
                                filled: true,
                                hintStyle: GoogleFonts.notoSansLao(
                                  color: Color.fromARGB(255, 75, 75, 75),
                                  height: 4,
                                  fontSize: 12,
                                ),
                                hintText: "Fill code number follow things"),
                          ),
                        ),
                      ),
                    ],
                  ),

                  // slide houb auto
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    height: 150,
                    width: 350,
                    child: CarouselSlider(
                      carouselController: controller,
                      options: CarouselOptions(
                        viewportFraction: 1,
                        aspectRatio: 2.0,
                        scrollDirection: Axis.horizontal,
                        autoPlay: true,
                        onPageChanged: (index, reason) =>
                            setState(() => ActiveIndex = index),
                      ),
                      items: _imagePaths
                          .map((item) => Container(
                                child: Container(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Stack(children: <Widget>[
                                      Image.asset(
                                        item,
                                        fit: BoxFit.cover,
                                        width: 1000,
                                      ),
                                      Positioned(
                                        bottom: 5,
                                        left: 150,
                                        right: 0,
                                        child: buildIndicator(),
                                      )
                                    ]),
                                  ),
                                ),
                              ))
                          .toList(),
                    ),
                  ),

                  // code title
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 25,
                        width: 5,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      NormalText(text: "ບໍລິການ"),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 12, right: 12),
                    height: 1,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  //end code title

                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 30, right: 30),
                          child: ICONBUTTON(
                            iconBT: Icon(Icons.storefront),
                            onPressed: () {},
                          )),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 30),
                          child: ICONBUTTON(
                              iconBT: Icon(Icons.all_inbox), onPressed: () {})),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 30),
                          child: ICONBUTTON(
                              iconBT: Icon(Icons.local_shipping),
                              onPressed: () {})),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: ICONBUTTON(
                              iconBT: Icon(Icons.local_atm_rounded),
                              onPressed: () {})),
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 45, right: 50),
                          child: TextIcon(text: "ສາຂາ")),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 35),
                          child: TextIcon(text: "ຝາກເຄື່ອງ")),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 30),
                          child: TextIcon(text: "ການຈັດສົ່ງ")),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 0),
                          child: TextIcon(text: "ເກັບປາຍທາງ")),
                    ],
                  ),
                  /////////////////////////////////////
                  // code title
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        height: 25,
                        width: 5,
                        color: Colors.yellow,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      NormalText(text: "ບໍລິການເສີມ"),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 12, right: 12),
                    height: 1,
                    width: double.infinity,
                    color: Colors.white,
                  ),
                  //end code title

                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 30, right: 30),
                          child: ICONBUTTON(
                            iconBT: Icon(Icons.calculate_outlined),
                            onPressed: () {},
                          )),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 30),
                          child: ICONBUTTON(
                              iconBT: Icon(Icons.gps_fixed_outlined),
                              onPressed: () {})),
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                          padding: EdgeInsets.only(left: 30, right: 30),
                          child: TextIcon(text: "ຄິດໄລ່ຂົນສົ່ງ")),
                      Padding(
                          padding: EdgeInsets.only(left: 0, right: 35),
                          child: TextIcon(text: "ແກ້ໄຂບັນຫາ")),
                    ],
                  ),
                ],
              ),
            )),
      ),
    );
  }

  Widget buildIndicator() => AnimatedSmoothIndicator(
        activeIndex: ActiveIndex,
        count: _imagePaths.length,
        effect: SlideEffect(
          dotColor: Colors.white,
          dotHeight: 7,
          dotWidth: 7,
          activeDotColor: Colors.orange,
        ),
      );
}
